require 'selenium-webdriver'

class HomePage

  InputField_Find = {xpath:'//div[@class="navbar-collapse d-none d-lg-flex"]//input[@placeholder="Find Freelancers"]'}

  Btn_FindJob={css:'.navbar-collapse.d-none.d-lg-flex span.glyphicon.air-icon-search.m-sm-left.m-0-right'}

  attr_reader :driver
  attr_reader :wait

  def driverInitializer(driver)
    @driver = driver
  end

  def waitInitializer(wait)
    @wait = wait
  end


  # Search freelancer
  def findFreelancers(keyword)
    input = wait.until {
      element =  driver.find_element(InputField_Find)
      element if element.displayed?
    }
    input.find_element(InputField_Find).send_keys @data_hash[keyword]
    input.find_element(Btn_FindJob).click

  end

end
