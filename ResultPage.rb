require 'selenium-webdriver'
require 'json'

class ResultPage

  # locator for result page
  FreeLen_name={css:'(//h4/a[@class="freelancer-tile-name"])[%d]'}
  Title_textField = {css:'h4 [title]:nth-child(%d)'}
  Skill_text ={css:'.skills-section span:nth-child(%d)'}
  Title_description ={css:'.col-xs-12 .d-lg-none p:nth-child(n)'}
  AttributeUserName={xpath:'(//span[@itemprop="name"])[2]'}

  # Global var
  attr_reader :driver
  attr_reader :wait

  @file = File.read( "DataSource.json")
  @data_hash = JSON.parse(@file)

  def driverInitializer(driver)
    @driver = driver
  end

  def waitInitializer(wait)
    @wait = wait
  end


  def getSearchResult
    elements = driver.find_elements(Title_textField)
    search_result = Hash.new
    for a in elements.length do
      result = Hash.new()

      result["UserName"]=driver.find_element(Title_textField % a ).text
      result["Skill"]=driver.find_element(Skill_text % a).text
      result["UserDesc"]=driver.find_element(Title_description % a).text

      # store all result in hash of hash
      return  search_result.store(a,result)

      putc search_result
    end
  end

  def checkFreelancerProfile
    profile =getSearchResult
    profile.each do |key,value|
      if value.include?@data_hash["SearchWord"]
        putc "Search keyword present #{key}"
      else
        putc "Search keyword not present #{key}"

        end
    end

  def clickFreelancerProfile
    driver.find_element(FreeLen_name % rand(1..10)).click
  end


    def verifyAttribute
    driver.find_element(AttributeUserName).text.include?@data_hash["SearchWord"]
    putc "User contains keyword"
    end



    end

end

