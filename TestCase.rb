require_relative 'HomePage'
require_relative 'ResultPage'
require 'selenium-webdriver'
require 'json'


# Read data from json file
  @file = File.read( "DataSource.json")
  @data_hash = JSON.parse(@file)

  @driver = Selenium::WebDriver.for(:chrome)
  @driver.manage.window.maximize
  @driver.manage.timeouts.page_load = 10
  @driver.navigate.to  "https://www.upwork.com/"
  @wait = Selenium::WebDriver::Wait.new(:timeout => 15)

  homePage= HomePage.new
  homePage.driverInitializer(@driver)
  homePage.waitInitializer(@wait)
  homePage.findFreelancers('SearchWord')   #Get keyword from external ile

  resultPage = ResultPage.new
  resultPage.driverInitializer(@driver)
  resultPage.waitInitializer(@wait)
  resultPage.getSearchResult
  resultPage.checkFreelancerProfile
  resultPage.clickFreelancerProfile

  # verify keyword is present or not
  resultPage.verifyAttribute








